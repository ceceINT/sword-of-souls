﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventsTL : MonoBehaviour {

    [Header("Add Unity Events Here!!")]
    public UnityEvent run;

    public void StartRun()
    {
        run.Invoke();
       
    }
   
}
