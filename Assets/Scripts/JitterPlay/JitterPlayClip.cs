using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class JitterPlayClip : PlayableAsset, ITimelineClipAsset
{
    public JitterPlayBehaviour template = new JitterPlayBehaviour ();

    public ClipCaps clipCaps
    {
        get { return ClipCaps.Blending; }
    }

    public override Playable CreatePlayable (PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<JitterPlayBehaviour>.Create (graph, template);
        return playable;
    }
}
