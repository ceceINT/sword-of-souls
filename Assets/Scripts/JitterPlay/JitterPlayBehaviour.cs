using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class JitterPlayBehaviour : PlayableBehaviour
{
    public bool enabled = true;
    public string name = "Hero";
    public AnimationCurve curve;

}
