﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoActive : MonoBehaviour {

    public GameObject Hero;
    public GameObject Skeleton;
    private void Start()
    {
        Play();
    }

    void Play () {
        Hero.SetActive(true);
        Skeleton.SetActive(true);
        Debug.Log("working)");
	}
}
