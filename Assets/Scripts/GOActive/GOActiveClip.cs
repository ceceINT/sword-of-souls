using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class GOActiveClip : PlayableAsset, ITimelineClipAsset
{
    public GOActiveBehaviour template = new GOActiveBehaviour ();

    public ClipCaps clipCaps
    {
        get { return ClipCaps.Blending; }
    }

    public override Playable CreatePlayable (PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<GOActiveBehaviour>.Create (graph, template);
        return playable;
    }
}
