using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class GOActiveBehaviour : PlayableBehaviour
{
    public string name = "Timeline";
    public bool enabled = true;
    public bool runInEditMode = false;
}
