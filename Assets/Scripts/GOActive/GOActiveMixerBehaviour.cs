using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class GOActiveMixerBehaviour : PlayableBehaviour
{
    string m_DefaultName;
    bool m_DefaultEnabled;
    bool m_DefaultRunInEditMode;

    MonoBehaviour m_TrackBinding;
    bool m_FirstFrameHappened;

    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        m_TrackBinding = playerData as MonoBehaviour;

        if (m_TrackBinding == null)
            return;

        if (!m_FirstFrameHappened)
        {
            m_DefaultName = m_TrackBinding.name;
            m_DefaultEnabled = m_TrackBinding.enabled;
            m_DefaultRunInEditMode = m_TrackBinding.runInEditMode;
            m_FirstFrameHappened = true;
        }

        int inputCount = playable.GetInputCount ();

        float totalWeight = 0f;
        float greatestWeight = 0f;
        int currentInputs = 0;

        for (int i = 0; i < inputCount; i++)
        {
            float inputWeight = playable.GetInputWeight(i);
            ScriptPlayable<GOActiveBehaviour> inputPlayable = (ScriptPlayable<GOActiveBehaviour>)playable.GetInput(i);
            GOActiveBehaviour input = inputPlayable.GetBehaviour ();
            
            totalWeight += inputWeight;

            if (inputWeight > greatestWeight)
            {
                m_TrackBinding.name = input.name;
                m_TrackBinding.enabled = input.enabled;
                m_TrackBinding.runInEditMode = input.runInEditMode;
                greatestWeight = inputWeight;
            }

            if (!Mathf.Approximately (inputWeight, 0f))
                currentInputs++;
        }

        if (currentInputs != 1 && 1f - totalWeight > greatestWeight)
        {
            m_TrackBinding.name = m_DefaultName;
            m_TrackBinding.enabled = m_DefaultEnabled;
            m_TrackBinding.runInEditMode = m_DefaultRunInEditMode;
        }
    }

    public override void OnGraphStop (Playable playable)
    {
        m_FirstFrameHappened = false;

        if (m_TrackBinding == null)
            return;

        m_TrackBinding.name = m_DefaultName;
        m_TrackBinding.enabled = m_DefaultEnabled;
        m_TrackBinding.runInEditMode = m_DefaultRunInEditMode;
    }
}
