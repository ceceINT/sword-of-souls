###Unity Version - 2017.3.0f3
## How to fix issues with unity timeline and play mode
 Resolving Transform issues in unity timeline
======
**ISSUE:** The animated character takes the transform of the timeline causing inconsistency between the timeline preview and the game window. 


**REPLICATION: **To replicate this issue, simply add your character to the animation track on the timeline, try previewing it and the transforms will change.


** Sidenote:** Once animation is added, it cannot be recorded over, so create a duplicate track and add transform animations to it.
 
** FIX - [Timeline resets the initial transform of the character so simply record one]** On the character- Add an Animator (Controller with at least an idle animation clip,Avatar and check Root Motion), On the timeline record the initial transform of the character or copy exact values from the inspector).
^^ The issue will be resolved when implementing regular transform animations but will return if you use a mocap through an animation clip. **To fix this** add the mocap clip to the timeline, create an override track and then record your needed transform or drag and drop the editor-animated transform as an override track.
